
function createCard(name, description, pictureURL,starts,ends) {
    return `

    <div class="g-col-4"
     <div class="shadow p-3 mb-5 bg-body-tertiary-rounded">
      <img src="${pictureURL}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">${starts} - ${ends}</div>
    </div>
    </div>

    `;
}


window.addEventListener('DOMContentLoaded',  async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
           console.error(e);
           alert(`${response.status} - ${response.statusText}`)
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailURL = `http://localhost:8000${conference.href}`;

                const detailResponse = await fetch(detailURL);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const picture_url = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts);
                    console.log(starts)
                    const ends = details.conference.ends;
                    const html = createCard(name, description, picture_url,starts,ends);


                    const column = document.querySelector('.col');
                    column.innerHTML += html;
                }


            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title')
            // nameTag.innerHTML = conference.name;

            // const detailURL = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailURL);
            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();
            //     console.log(details)
            //     const description = details.conference.description;
            //     const descTag = document.querySelector('.card-text');
            //     descTag.innerHTML = description;

            //     const picture = details.conference.location.picture_url
            //     console.log(picture)
            //     const imgTag = document.querySelector('.card-img-top')
            //     imgTag.src = picture;
            }
        }
    } catch (e) {
        console.error(e);
    }




});
